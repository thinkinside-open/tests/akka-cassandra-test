package thinkin.test

import akka.actor.typed.{ActorSystem, ActorRef}
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives._
import akka.stream.scaladsl.{Source, Keep, Sink}
import akka.stream.typed.scaladsl.ActorSink

import com.datastax.oss.driver.api.core._
import thinkin.test.utils._
import kamon.Kamon
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import java.net.InetSocketAddress
import scala.util.Random
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.jmx.JmxReporter
import io.micrometer.core.instrument.search.RequiredSearch
import thinkin.test.actors.DurableLastStringsActor
import com.typesafe.config.ConfigFactory
import kamon.prometheus.PrometheusReporter
import com.typesafe.config.ConfigRenderOptions
import thinkin.test.actors.DurableLastStringsActor
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Broadcast
import akka.NotUsed
import akka.actor.typed.PostStop
import akka.actor.typed.Behavior
object AkkaCassandraTest extends App {
  val conf = ConfigFactory.load()
  val appConf = conf.getConfig("app")
  val log = LoggerFactory.getLogger(this.getClass())
  log.info(
    "Akka snapshot: ",
    conf
      .getConfig("akka.persistence.cassandra.snapshot")
      .root()
      .render(ConfigRenderOptions.concise())
  )
  val frequency = {
    val jd = appConf.getDuration("frequency")
    jd.toNanos().nano
  }
  val messageSize = appConf.getInt("message-size")
  val historySize = appConf.getInt("history-size")
  val persistenceId = appConf.getString("persistence-id")
  val snapshotEvery = appConf.getInt("snapshots.every-n-messages")
  val keepNSnapshots = appConf.getInt("snapshots.keep-n-snapshots")
  val actorNumber = appConf.getInt("actors")

  log.info(
    s"Starting with parameters: [persistenceId=${persistenceId}] [frequency=${frequency}] [messageSize=${messageSize}] [historySize=${historySize}]"
  )
  Kamon.init()
  val kamonPrometheusReporter = new PrometheusReporter
  Kamon.registerModule("prometheus", kamonPrometheusReporter)

  implicit val system = ActorSystem(Behaviors.empty, "my-system")
  import akka.actor.{DeadLetter, Props}
  import akka.actor.typed.eventstream.EventStream;

  val deadLettersListener: Behavior[DeadLetter] = Behaviors.receive { (ctx, msg) =>
    msg match {
      case DeadLetter(message, sender, recipient) =>
        log.info(s"***Dead letter received*** ($message, $sender, $recipient)")
        println(s"***Dead letter received*** ($message, $sender, $recipient)")
        Behaviors.same
    }
  }
  implicit val executionContext = system.executionContext

  val deadLettersActor: ActorRef[DeadLetter] =
    system.systemActorOf(deadLettersListener, "deadLettersListener")
  system.eventStream.tell(EventStream.Subscribe[DeadLetter](deadLettersActor))
  Kamon.gauge("started_actors").withoutTags().increment(actorNumber)
  val activeActorsGauge = Kamon.gauge("active_actors").withoutTags()

  def createActor(id: Int): ActorRef[DurableLastStringsActor.Command] = {
    activeActorsGauge.increment()
    system.systemActorOf(
      DurableLastStringsActor
        .lastStringsByPrefix(historySize, s"$persistenceId-$id", snapshotEvery, keepNSnapshots)
        .receiveSignal { case (_, PostStop) =>
          activeActorsGauge.decrement()
          log.info(s"Worker $id stopped")
        },
      s"$persistenceId-$id"
    )
  }
  val stringsByInitials: ActorRef[DurableLastStringsActor.Command] = system

  // needed for the future flatMap/onComplete in the end

  val reporter =
    JmxReporter
      .forRegistry(
        CassandraSessionWithMetricsProvider.jmxRegistry.getDropwizardRegistry()
      )
      .inDomain("com.datastax.oss.driver")
      .build();

  reporter.start();

  def reqRespInfo(req: HttpRequest): RouteResult => Unit = {
    val reqTimestamp = System.currentTimeMillis()
    val uri = req.getUri().asScala()
    val queryRepr = uri
      .query()
      .toList
      .sorted
      .map { case (name, value) => s"$name=$value" }
      .mkString(" ")

    log.info(
      s"REQUEST STARTED ${req.method.name} $uri [${req.entity.contentType}] query [ $queryRepr ]"
    )

    {
      case res: RouteResult.Complete =>
        val respTimestamp = System.currentTimeMillis()
        val elapsedMs = respTimestamp - reqTimestamp
        val message =
          s"REQUEST FINISHED ${req.method.name} ${uri.path} [${req.entity.contentType}] query [ $queryRepr ] [${res.response.status}](${elapsedMs}ms)"
        log.info(message)

      case rej: RouteResult.Rejected =>
        val respTimestamp = System.currentTimeMillis()
        val elapsedMs = respTimestamp - reqTimestamp
        val message =
          s"REQUEST FINISHED ${req.method.name} ${uri.path} [${req.entity.contentType}] query[ $queryRepr ][REJECTED](${elapsedMs}ms): ${rej.rejections
            .mkString("; ")}"
        log.info(message)
    }
  }

  val route = path("metrics") {
    get {
      complete(
        List(
          CassandraSessionWithMetricsProvider.prometheusRegistry.scrape(),
          "## ^---- CASSANDRA, KAMON-----V ",
          kamonPrometheusReporter.scrapeData()
        ).mkString
      )
    }
  }

  val port = appConf.getInt("metrics.port")
  val bindingFuture = Http().newServerAt("0.0.0.0", port).bind(route)

  bindingFuture.onComplete { v =>
    log.info(s"Server started $v")
  } // and shutdown when done

  import scala.concurrent.duration._

  val source = if (frequency == Duration.Zero) {
    Source.fromIterator(() => Iterator.continually(()))
  } else {
    Source.tick(Duration.Zero, frequency, ())
  }

  val heartbeat = appConf.getDuration("heartbeat").toNanos.nanos
  val heartbeatSource = Source.tick(heartbeat, heartbeat, "heartbeat")

  def createSinkWithActor(id: Int): Sink[String, NotUsed] = {
    val counter = Kamon.counter("sinked_messages").withTag("sinkId", id)
    val f =
      Flow.fromFunction { (s: String) =>
        counter.increment()
        s
      }

    f.to(
      ActorSink
        .actorRefWithBackpressure(
          createActor(id),
          messageAdapter = { (a: ActorRef[DurableLastStringsActor.AckType], m: String) =>
            m match {
              case "heartbeat" => DurableLastStringsActor.HeartbeatCommand(a)
              case _           => DurableLastStringsActor.StringCommand(m, a)
            }
          },
          ackMessage = DurableLastStringsActor.Ack,
          onInitMessage = (a: ActorRef[DurableLastStringsActor.AckType]) =>
            DurableLastStringsActor.StringCommand("", a),
          onCompleteMessage = DurableLastStringsActor.CompleteCommand,
          onFailureMessage = _ => DurableLastStringsActor.CompleteCommand
        )
    )
  }

  val sink = if (actorNumber <= 0) {
    Flow.apply[String].toMat(Sink.ignore)(Keep.left)
  } else if (actorNumber == 1) {
    createSinkWithActor(0)
  } else {
    Sink.combine(
      createSinkWithActor(0),
      createSinkWithActor(1),
      2.until(actorNumber).map(createSinkWithActor).toSeq: _*
    )(Broadcast[String](_))
  }

  val v = source
    .mergePreferred(heartbeatSource, true)
    .map { _ =>
      Random.alphanumeric.take(messageSize).mkString
    }
    .runWith(
      sink
    )
}
