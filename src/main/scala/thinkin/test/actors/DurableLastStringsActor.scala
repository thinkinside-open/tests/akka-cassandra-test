package thinkin.test.actors

import akka.persistence.typed.state.scaladsl._
import akka.persistence.typed.scaladsl.Effect

import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.EventSourcedBehavior
import akka.persistence.typed.scaladsl._

import org.slf4j.LoggerFactory
import akka.actor.typed.ActorRef
import akka.actor.typed.PostStop
import akka.actor.typed.scaladsl.Behaviors

trait CborSerializable

object DurableLastStringsActor {
  sealed trait Command extends CborSerializable
  sealed trait AckType
  case object Ack extends AckType
  case class StringCommand(newString: String, replyTo: ActorRef[AckType]) extends Command
  case object CompleteCommand extends Command
  case class HeartbeatCommand(replyTo: ActorRef[AckType]) extends Command

  final case class Event(newString: String) extends CborSerializable

  final case class State(valuesByInitial: Map[Char, List[String]]) extends CborSerializable {
    def add(historySize: Int, s: String): State = State {
      val initial = s.head
      valuesByInitial + (initial -> (s :: valuesByInitial
        .getOrElse(initial, Nil)
        .take(historySize - 1)))
    }
  }

  val log = LoggerFactory.getLogger(this.getClass)

  def lastStringsByPrefix(
      historySize: Int,
      persistenceId: String,
      snapshotEvery: Int,
      keepNSnapshots: Int
  ): EventSourcedBehavior[Command, Event, State] = {
    EventSourcedBehavior
      .apply[Command, Event, State](
        persistenceId = PersistenceId.ofUniqueId(persistenceId),
        emptyState = State(Map.empty),
        commandHandler = (state, command) =>
          command match {
            case StringCommand("", replyTo) =>
              Effect.unhandled.thenReply(replyTo)(_ => Ack)
            case StringCommand(s, replyTo) =>
              Effect.persist(Event(s)).thenReply(replyTo)(_ => Ack)
            case CompleteCommand =>
              log.info("CompleteCommandReceived")
              Effect.stop()
            case HeartbeatCommand(replyTo) =>
              log.info(s"Actor [$persistenceId] is still alive")
              Effect.unhandled.thenReply(replyTo)(_ => Ack)
          },
        eventHandler = { case (state, Event(s)) =>
          state.add(historySize, s)
        }
      )
      .withRetention(
        RetentionCriteria
          .snapshotEvery(
            numberOfEvents = snapshotEvery,
            keepNSnapshots = keepNSnapshots
          )
          .withDeleteEventsOnSnapshot
      )
      .withRecovery(Recovery.default)

  }
}
