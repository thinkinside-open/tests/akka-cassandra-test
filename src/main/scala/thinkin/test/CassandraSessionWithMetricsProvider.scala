package thinkin.test

import akka.stream.alpakka.cassandra.CqlSessionProvider
import com.datastax.oss.driver.api.core.CqlSession
import scala.concurrent.{ExecutionContext, Future}
import io.micrometer.core.instrument.composite.CompositeMeterRegistry
import io.micrometer.prometheus.PrometheusMeterRegistry
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.jmx.JmxMeterRegistry
import io.micrometer.core.instrument.Clock
import java.net.InetSocketAddress

import thinkin.test.utils._
import org.slf4j.LoggerFactory


object CassandraSessionWithMetricsProvider {
  lazy val jmxRegistry = new JmxMeterRegistry(s => null, Clock.SYSTEM)
  lazy val prometheusRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT)
}

class CassandraSessionWithMetricsProvider() extends CqlSessionProvider {
  private val log = LoggerFactory.getLogger(this.getClass)
  
  override def connect()(implicit ec: ExecutionContext): Future[CqlSession] = {
    log.info("Using custom CQLSession builder")
    val metricRegistry = {
        val r = new CompositeMeterRegistry();
        r.add(CassandraSessionWithMetricsProvider.jmxRegistry);
        r.add(CassandraSessionWithMetricsProvider.prometheusRegistry)
        r
    }

    CqlSession.builder()
      .withMetricRegistry(metricRegistry)
      .buildAsync()
  }
}
