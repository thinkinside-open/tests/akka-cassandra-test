package thinkin.test
import com.datastax.oss.driver.api.core._
import com.datastax.oss.driver.api.core.cql._
import scala.language.implicitConversions
import scala.concurrent._
import java.util.concurrent.CompletionStage

package object utils {

  implicit def cassandraFutureToScalaFuture[T](
      cs: CompletionStage[T]
  ): Future[T] = {
    val p = Promise[T]()
    cs.whenComplete { case (result, error) =>
      // If error is not null, then future failed.
      if (error ne null) p.failure(error)

      // Otherwise, it succeeded with result.
      else p.success(result)
    }

    p.future
  }
  def ftraverse[A, B](
      xs: Seq[A]
  )(f: A => Future[B])(implicit ec: ExecutionContext): Future[Seq[B]] = {
    if (xs.isEmpty) Future successful Seq.empty[B]
    else f(xs.head) flatMap { fh => ftraverse(xs.tail)(f) map (r => fh +: r) }
  }

}
