import sbt._
import sbt._
import sbt.Keys._

import java.io.FileOutputStream
import scala.util.{Failure, Success, Try}

object Dependencies {

  object Cassandra {
    val version = "4.13.0"
    val driver = "com.datastax.oss" % "java-driver-core" % version
    val driverShaded = "com.datastax.oss" % "java-driver-core-shaded" % version
    val mapperRuntime = "com.datastax.oss" % "java-driver-mapper-runtime" % version
    val queryBuilder = "com.datastax.oss" % "java-driver-query-builder" % version
    val nativeProtocol = "com.datastax.oss" % "native-protocol" % "1.4.10"
    val micrometer = "io.micrometer" % "micrometer-registry-prometheus" % "1.7.4"
    val micrometerJmx = "io.micrometer" % "micrometer-registry-jmx" % "1.7.4"
    val cassandraMicrometer = "com.datastax.oss" % "java-driver-metrics-micrometer" % version
    val alpakkaCassandra = "com.lightbend.akka" %% "akka-stream-alpakka-cassandra" % "2.0.2"
    val all = List(
      driver,
      nativeProtocol,
      driverShaded,
      mapperRuntime,
      queryBuilder,
      micrometer,
      micrometerJmx,
      cassandraMicrometer,
      alpakkaCassandra
    )
  }

  object Akka {
    val akkaVersion = "2.6.16"
    val all = Seq(
      "com.typesafe.akka" %% "akka-persistence-cassandra" % "1.0.5",
      "com.typesafe.akka" %% "akka-serialization-jackson" % akkaVersion,
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.12.2",
      "com.typesafe.akka" %% "akka-stream-typed" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence-typed" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence-query" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion
    )
  }

  object logging {
    val slf4j = "org.slf4j" % "slf4j-api" % "1.7.26"
    val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
    val log4jOverSlf4j = "org.slf4j" % "log4j-over-slf4j" % "1.7.26"
    val jclOverSlf4j = "org.slf4j" % "jcl-over-slf4j" % "1.7.26"
    val all = Seq(slf4j, logback, log4jOverSlf4j, jclOverSlf4j)
  }

}
