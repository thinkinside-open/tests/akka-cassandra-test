import org.checkerframework.checker.units.qual.A
import Dependencies._
import sbt.inConfig
name := "akka-cassandra-test"

version := "0.1"

scalaVersion := "2.12.12"

scalacOptions ++= Seq("-Xmax-classfile-name", "100", "-feature")

javacOptions ++= Seq(
  "-source",
  "1.8",
  "-target",
  "1.8"
)

libraryDependencies ++= Seq(
  "io.kamon" %% "kamon-core" % "2.2.2",
  "io.kamon" %% "kamon-bundle" % "2.2.2",
  "io.kamon" %% "kamon-cassandra" % "2.2.2",
  "io.kamon" %% "kamon-system-metrics" % "2.2.2",
  "io.kamon" %% "kamon-status-page" % "2.2.2",
  "io.kamon" %% "kamon-prometheus" % "2.2.2",
  "com.typesafe" % "config" % "1.3.0",
  "io.dropwizard.metrics" % "metrics-jmx" % "4.0.2",
  "com.typesafe.akka" %% "akka-http" % "10.2.6"
) ++ Cassandra.all ++ Akka.all ++ logging.all

enablePlugins(PackPlugin)

packMain := Map("akka-cassandra-test" -> "thinkin.test.AkkaCassandraTest")
packGenerateWindowsBatFile := false
packJvmOpts := Map(
  "akka-cassandra-test" ->
    Seq(
      s"-javaagent:$$PROG_HOME/kanela-agent-1.0.9.jar"
    )
)

packExtraClasspath := Map("akka-cassandra-test" -> Seq("${PROG_HOME}"))
