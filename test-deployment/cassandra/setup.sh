#!/bin/bash
echo "*** Step 1: Install Docker"
apt-get install     apt-transport-https     ca-certificates     curl     gnupg     lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io docker-compose

echo "*** Step 2: Create the configuration for a cassandra cluster with a single seed node"

DEFAULT_ADDRESS=`/sbin/ifconfig ens10 |grep "inet " | awk '{ print $2 }'`

read -p "Which is the IP address of this host on the private network (Default: $DEFAULT_ADDRESS): " ADDRESS
: ${ADDRESS:=$DEFAULT_ADDRESS}
echo
echo "CASSANDRA_BROADCAST=$ADDRESS" > .env

read -p "Is this the seed node [Yy/Nn]? (reply Yes only for a node) " -n 1 -r
echo
if [[ ! "$REPLY" =~ ^[Yy]$ ]]; then
    read -p "Seed ip address (private network): " SEED
    echo
    echo "CASSANDRA_SEEDS=$SEED" >> .env
fi

echo "*** .env file created"

mkdir -p cassandra-exporter
echo "*** Step 3: Downloading metric exporter agent from:" 
echo "    https://github.com/instaclustr/cassandra-exporter/releases/download/v0.9.10/cassandra-exporter-agent-0.9.10.jar"
echo -n "..."
curl -qs -o"cassandra-exporter/cassandra-exporter-agent-0.9.10.jar"  "https://github.com/instaclustr/cassandra-exporter/releases/download/v0.9.10/cassandra-exporter-agent-0.9.10.jar"
echo " Done"

echo "*** Step 4: Setting sysctl network parameters"

cat <<EOF >> /etc/sysctl.conf
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.core.rmem_default = 16777216
net.core.wmem_default = 16777216
net.core.optmem_max = 40960
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 65536 16777216
EOF

sysctl --system


echo "Once all the node have been configured you must use 'docker-compose' to start one node at the time."
echo "Start from the seed node, then move to the others and wait that the current node has joined the cluster"
echo "before passing to the next one."
echo 
echo "Once the cluster is ready you can use 'docker exec' to run cqlsh and create the 'test' keyspace:"
echo "  CREATE  KEYSPACE test WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 2 }; "