# Cassandra test deployment #

The cassandra test deployment is based on Hetzner cloud (Cassandra Crossing project) by the following hosts.

| node1 | CX31 / 80 |
| node2 | CX31 / 80 |
| node3 | CX31 / 80 |
| node4 | CX31 / 80 |
| tester      | CPX11 / 40 GB |
| monitoring  | CPX11 / 40 GB |

in each of the server in the `/etc/hosts` file the hosts with the `.cassandra` domain are listed with the corresponding IP Address
(this is mandatory for the monitoring server)

## Monitoring Host

Prometheus and Grafana are installed from `apt` as systemctl services. The versions used in the last test deployment are:

* Prometheus: 2.15.2
* Grafana: 8.2

In `monitoring` folder you can find configuration files and a Grafana Dashboard that collect client and
server metrics.

The `setup.sh` script in such directory will install, configure and start `grafana` and `prometheus`. You will need to add the
Grafana Dashboard manually, you will be reminded at the end of the script.

## Cassandra Hosts

In folder `cassandra` there is a script for the setup, a `docker-compose.yml` file and some other configuration files.

For each node the setup script must be run.
The `./setup.sh` script will help you in preparing the configuration for running your Cassandra node.
The test deployment expects to have 4 nodes, exactly one of them must be the cluster seed node (for each host you need to answer whether that node is a seed node, remember to reply 'yes' for only one of them).

At the end of the script you will get info on how to start the cluster and how to create the required `test` keyspace (it should be done
only once on any node, when the cluster is ready).