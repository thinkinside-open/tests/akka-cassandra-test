
echo "*** You need to add to the /etc/hosts file the following hosts with the right addresses:"
echo "    node1.cassandra # Host running Cassandra node1"
echo "    node2.cassandra # Host running Cassandra node2"
echo "    node3.cassandra # Host running Cassandra node3"
echo "    node4.cassandra # Host running Cassandra node4"
echo "    tester.cassandra # Host running the tester"
echo

read -p "Press <enter> when you have created the entries in /etc/hosts" WAIT
echo

echo "*** Installing prometheus..."
apt-get update
apt install -y prometheus
cp prometheus.yml /etc/prometheus
echo 'ARGS="--storage.tsdb.retention.size 20G"' > /etc/default/prometheus

echo "*** Installing grafana..."
apt-get install -y apt-transport-https
echo "deb https://packages.grafana.com/oss/deb stable main" | tee -a /etc/apt/sources.list.d/grafana.list
apt-get update
wget -q -O - https://packages.grafana.com/gpg.key | apt-key add -
apt-get update
apt install -y grafana

echo "*** starting services..."
systemctl daemon-reload
systemctl enable prometheus
systemctl start prometheus
sudo systemctl start grafana-server
sudo systemctl enable grafana-server.service

echo
echo 
echo
echo "***"
echo "*** If you want to disable the login page on Grafana just add the following lines to /etc/grafana/grafana.ini and restart the server"
echo "disable_login_form = true"
echo "disable_signout_menu = true"
echo
echo "***"
echo "*** Import the `grafana-dashboard.json` file to grafana for having a exploratory Cassandra client/server dashboard"