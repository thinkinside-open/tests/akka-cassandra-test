# Cassandra Akka Persistence test

This repository contains the code for creating a given amount of persisted typed actors with the goal of testing the interaction between this kind of clients and the Cassandra nodes.

## Installation

The folder `test-deployment` contains the description for the creation of a test deployment made of 6 hosts:

* 1 monitoring
* 1 tester node
* 4 cassandra nodes

## What we found

Tests where tried with a number of actors between 20-50. In both cases, monitoring with grafana the number of alive actors, you can see that
the number decreases. This is due to timeouts while trying to persist the events in the tester application.

The only correlation noted so far is that often the timeout occurs when an high cross-node latency is present (look the 99th percentile). Why this
is the case need further investigation, probably checking the cassandra nodes' logs. Occurrences of this problem have been noticed also in production.


